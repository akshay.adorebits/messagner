import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../screens/LoginScreen';
import ChatScreen from '../screens/ChatScreen';
import { Button,Text ,TouchableOpacity} from 'react-native';
import LogoutScreen from '../screens/LogoutScreen';

const Stack = createStackNavigator();

export const AuthStackScreen = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen
          name="login"
          component={LoginScreen}
          options={{headerShown : false}}
        />
        <Stack.Screen 
            name="chat" 
            component={ChatScreen} 
            />
      </Stack.Navigator>
  );
};

export const AppStackScreen = ({navigation}) => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name="chat" 
                component={ChatScreen} 
                options={{
                    headerRight: () => (
                      <TouchableOpacity
                        onPress={() => navigation.navigate('logout')}
                      ><Text>Logout</Text></TouchableOpacity>
                    ),
                  }}
            />
            <Stack.Screen 
                name="logout"
                component={LogoutScreen}
            />
            <Stack.Screen
                name="login"
                component={LoginScreen}
                options={{headerShown : false}}
                />
        </Stack.Navigator>
    )
}

const rootNavigation = ({user}) =>{
    return (
        <NavigationContainer>
            <Stack.Navigator>
                 { user ? 
                    <Stack.Screen 
                        name="App" 
                        component={App} 
                        options={{headerShown : false}}
                    /> : 
                    <Stack.Screen 
                        name="Auth" 
                        component={Auth} 
                        options={{headerShown : false}}
                    />
                } 
            </Stack.Navigator>
        </NavigationContainer>
    )
}