import React, { Component } from 'react';
import { StyleSheet,View, Text,ImageBackground,TextInput,TouchableHighlight,Button,ActivityIndicator} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class LoginScreen extends Component{

    constructor(props){
        super(props)
        this.state = {
            username : ''
        }
    }

    _SignIN = async() => {
      if(this.state.username != ''){
        await AsyncStorage.setItem("username",this.state.username);
        this.props.navigation.navigate('chat')
      }else{
        alert("Enter Username")
      }
    }

    renderAcessButton() {
        if (this.props.signInLoading) {
          return (<ActivityIndicator size="large" color="#00ff00" />)
        }
        return (<Button title="Log in" color='white' onPress={() => this._SignIN()} />)
      }

    render(){
        return(
            <ImageBackground source={require('../images/ic_log_in_background.png')} style={{ flex: 1, width: null }}>
                <View style={styles.container}>
                    <View style={styles.title}>
                        <Text style={styles.textTitle}>Messanger</Text>
                    </View>

                    <View style={styles.formGroup}>
                        <TextInput
                            value={this.state.email}
                            onChangeText={username => this.setState({username})}
                            placeholder='Username'
                            placeholderTextColor='#fff'
                            style={styles.textInput}
                            returnKeyType="next"
                            // onSubmitEditing={() => this.passwordInput.focus()}
                        />
                    </View>

                    <View style={styles.btnLogIn}>
                        { this.renderAcessButton() }
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 10
    },
    title: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    textTitle: {
     fontSize: 30,
     color: '#fff'
    },
    formGroup: {
      flex: 2,
    },
    textInput: {
      color : 'white',
      fontSize: 18,
      height: 45,
      borderBottomWidth : 1,
      borderBottomColor:'white'
    },
    textRegister: {
      fontSize: 16,
      color: '#A0A0A0'
    },
    btnLogIn: {
      flex: 1,
    }
});
