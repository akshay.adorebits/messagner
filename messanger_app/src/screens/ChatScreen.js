import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react';
//import { StyleSheet,View,TextInput,Text, Dimensions, ScrollView,KeyboardAvoidingView, Platform ,TouchableHighlight,ListView} from 'react-native';
import { View, Text, TextInput, Image, TouchableOpacity, ListView, FlatList,KeyboardAvoidingView,ScrollView } from 'react-native';

import io from 'socket.io-client';

//const { width, height } = Dimensions.get('window');

export default class ChatScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            chatmessage : '',
            chatmessages : []
        }
    }

    componentDidMount() {
        this.socket = io("http://localhost:3000");
        // this.socket.on("chat message", msg => {
        //     this.setState({ chatmessages : [...this.state.chatmessages, msg] })

        // });   
        this.socket.on("chat message", ({name,message}) => {
            this.setState({ chatmessages : [...this.state.chatmessages, {name,message}] })

        });  
       
        this.getUsername();
    }

    // submitChat = async() => {
    //     //var name = await AsyncStorage.getItem("username")
    //     this.socket.emit("chat message",this.state.chatmessage);
    //     this.setState({chatmessage : ''})
    // }
    getUsername = async() => {
        let username = await AsyncStorage.getItem("username");
        this.setState({username : username})
    }
    submitChat = async() => {

        if(this.state.chatmessage != ''){
            var name = await AsyncStorage.getItem("username");
        // var name = this.state.username;
            var message = this.state.chatmessage;
            this.socket.emit("chat message", { name ,message });
            this.setState({chatmessage : ''})
        }
        
    }

    _renderItem = ({item,index})=>{
        if(item.name == this.state.username ){
            return(
                <View style={{ alignItems: 'flex-end', marginTop: 5, marginLeft: 40, marginBottom: 5 }} key={index}>
                    <View style={{ backgroundColor: '#dbf5b4', borderRadius: 10 }}>
                        <Text style={{ fontSize: 16, color: '#0d0d0d', padding: 8 }}>{item.message}</Text>
                        {/*<Text style={{ fontSize: 12, color: '#999999', marginRight: 10, marginBottom: 5, textAlign: 'right' }}>11:23 PM</Text>*/}
                    </View>
                </View> 
            )
        }
        return(
            <View style={{ alignItems: 'flex-start', marginTop: 5, marginRight: 40, marginBottom: 5 }}>
                <View style={{ backgroundColor: '#bfbfbf', borderRadius: 10 }}>
                    <Text style={{ fontSize: 16, color: '#0d0d0d', padding: 8, elevation: 1 }}>{item.message}</Text>
                    {/* <Text style={{ fontSize: 12, color: '#999999', marginLeft: 10, marginBottom: 5 }}>00:25 PM</Text> */}
                </View>
            </View>
        )
            
        
        
    }
    render(){
        
        // const chatmessages = this.state.chatmessages.map(message=>(
        //     <View style={{ alignItems: 'flex-end', marginTop: 5, marginLeft: 40, marginBottom: 5 }} key={message}>
        //         <View style={{ backgroundColor: '#dbf5b4', borderRadius: 10 }}>
        //             <Text style={{ fontSize: 16, color: '#0d0d0d', padding: 8 }}>{message}</Text>
        //             {/*<Text style={{ fontSize: 12, color: '#999999', marginRight: 10, marginBottom: 5, textAlign: 'right' }}>11:23 PM</Text>*/}
        //         </View>
        //     </View> 
            
        // ));
        return(
            <KeyboardAvoidingView style={{flexGrow: 1, flexShrink: 1 }} behavior={Platform.OS == 'ios' ? "padding" : 'height'}  keyboardVerticalOffset={80}>
                <View style={{ flex: 1, backgroundColor: '#eee4dc', padding: 10 }} >
                    <View style={{ flex: 1, paddingBottom: 20 }}>
                        {/* {chatmessages} */}
                        <FlatList 
                            data = {this.state.chatmessages}
                            keyExtractor={(item,index)=>index.toString()}
                            renderItem={this._renderItem}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', height: 60 }}>
                        <TextInput
                            placeholder="Type a message..."
                            onChangeText={(chatmessage)=>this.setState({chatmessage})}
                            underlineColorAndroid='transparent'
                            value={this.state.chatmessage}
                            style={{ flex: 4, backgroundColor: '#fff', fontSize: 15, borderRadius: 30,paddingHorizontal : 10 }}
                        />
                
                        <TouchableOpacity
                            onPress={()=>this.submitChat() }>
                            <Image source={require('../images/ic_button_send_sms.png')} style={{ width: 60, height: 60, marginLeft: 5 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}