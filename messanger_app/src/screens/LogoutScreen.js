import React, { Component } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class LogoutScreen extends Component{
    constructor(props){
        super(props);
    }
    UNSAFE_componentWillMount(){
        this.logout();
    }
    logout = async() =>{
        await AsyncStorage.removeItem("username");

        this.props.navigation.navigate('login')
    }
    render(){
        return(
            <View></View>
        )
    }
}