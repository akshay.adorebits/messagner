import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  LogBox
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {AppStackScreen,AuthStackScreen} from './src/navigation/rootNavigation';
import ChatScreen from './src/screens/ChatScreen';

//LogBox.ignoreAllLogs();
export default class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      user : ''
    }
    this.checkLogin();
  }

  checkLogin = async()=>{
   // await AsyncStorage.removeItem("username")
    var username = await AsyncStorage.getItem("username") 
    this.setState({user : username})
  }
  render(){
    return(
      <SafeAreaView style={styles.container}>
        {/* <ChatScreen /> */}
        <NavigationContainer>
          <RootStackScreen user={this.state.user}/>
        </NavigationContainer>
        
      </SafeAreaView>
    )
  }
};

const RootStack = createStackNavigator();
const RootStackScreen = ({ user }) => (
    console.log("User Token 1", user),
    console.log("User Token 2", user),
    <RootStack.Navigator headerMode="none">
        {user ? (
            <RootStack.Screen
                name="App"
                component={AppStackScreen}
                options={{
                    animationEnabled: false
                }}
            />)
            :
            <RootStack.Screen
                name="Auth"
                component={AuthStackScreen}
                options={{
                    animationEnabled: false
                }}
            />
        }
    </RootStack.Navigator>
);

const styles = StyleSheet.create({
  container : {
    flex:1,
  },
  chat:{
   
  }
});
