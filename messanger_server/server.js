const express = require('express');
const http = require('http');
const socketio = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketio.listen(server);
const port = 3000;

// io.on("connection",socket=>{
//     console.log("Connection");
//     socket.on("chat message",msg=>{
//         console.log("message is",msg)
//         io.emit("chat message",msg)
//     })
// });

    io.on("connection",socket=>{
        console.log("Connection");
        socket.on("chat message",({name,message})=>{
            console.log("message is",{name,message})
            io.emit("chat message",{name,message})
        })
    });

server.listen(port,()=>{console.log('server start')})
